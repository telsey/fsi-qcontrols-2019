﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="19008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="FSI Library.lvlibp" Type="LVLibp" URL="../FSI Library.lvlibp">
			<Item Name="Browse Dialog" Type="Folder">
				<Item Name="Browse Dialog.lvlib" Type="Library" URL="../FSI Library.lvlibp/Browse Dialog/Browse Dialog.lvlib"/>
			</Item>
			<Item Name="Units" Type="Folder">
				<Item Name="bar.lvclass" Type="LVClass" URL="../FSI Library.lvlibp/Units/Pressure/bar/bar.lvclass"/>
				<Item Name="inH2O @68F.lvclass" Type="LVClass" URL="../FSI Library.lvlibp/Units/Pressure/inH2O 68F/inH2O @68F.lvclass"/>
				<Item Name="kPa.lvclass" Type="LVClass" URL="../FSI Library.lvlibp/Units/Pressure/kPa/kPa.lvclass"/>
				<Item Name="Pressure.lvclass" Type="LVClass" URL="../FSI Library.lvlibp/Units/Pressure/Pressure/Pressure.lvclass"/>
				<Item Name="psi.lvclass" Type="LVClass" URL="../FSI Library.lvlibp/Units/Pressure/psi/psi.lvclass"/>
				<Item Name="Unit.lvlib" Type="Library" URL="../FSI Library.lvlibp/Units/Unit/Unit.lvlib"/>
			</Item>
			<Item Name="Convert File Extension (String)__ogtk.vi" Type="VI" URL="../FSI Library.lvlibp/1abvi3w/user.lib/_OpenG.lib/file/file.llb/Convert File Extension (String)__ogtk.vi"/>
			<Item Name="Delete Elements from 1D Array (String)__ogtk.vi" Type="VI" URL="../FSI Library.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (String)__ogtk.vi"/>
			<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../FSI Library.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
			<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="../FSI Library.lvlibp/1abvi3w/vi.lib/express/express shared/ex_CorrectErrorChain.vi"/>
			<Item Name="Filter 1D Array with Scalar (String)__ogtk.vi" Type="VI" URL="../FSI Library.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (String)__ogtk.vi"/>
			<Item Name="Get LV Class Default Value.vi" Type="VI" URL="../FSI Library.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Get LV Class Default Value.vi"/>
			<Item Name="List Directory__ogtk.vi" Type="VI" URL="../FSI Library.lvlibp/1abvi3w/user.lib/_OpenG.lib/file/file.llb/List Directory__ogtk.vi"/>
			<Item Name="Remove Duplicates from 1D Array (String)__ogtk.vi" Type="VI" URL="../FSI Library.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (String)__ogtk.vi"/>
			<Item Name="Reorder 1D Array2 (String)__ogtk.vi" Type="VI" URL="../FSI Library.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Reorder 1D Array2 (String)__ogtk.vi"/>
			<Item Name="Search 1D Array (String)__ogtk.vi" Type="VI" URL="../FSI Library.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Search 1D Array (String)__ogtk.vi"/>
			<Item Name="Sort 1D Array (I32)__ogtk.vi" Type="VI" URL="../FSI Library.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Sort 1D Array (I32)__ogtk.vi"/>
			<Item Name="subFile Dialog.vi" Type="VI" URL="../FSI Library.lvlibp/1abvi3w/vi.lib/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
			<Item Name="Trim Whitespace (String)__ogtk.vi" Type="VI" URL="../FSI Library.lvlibp/1abvi3w/user.lib/_OpenG.lib/string/string.llb/Trim Whitespace (String)__ogtk.vi"/>
		</Item>
		<Item Name="PathComboBox.lvclass" Type="LVClass" URL="../PathComboBox/PathComboBox.lvclass"/>
		<Item Name="Unit Numeric.lvclass" Type="LVClass" URL="../Unit Numeric/Unit Numeric.lvclass"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="ComboBox.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/QSI/QControl Classes/ComboBox/ComboBox.lvclass"/>
				<Item Name="Control.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/QSI/QControl Classes/Control/Control.lvclass"/>
				<Item Name="Digital.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/QSI/QControl Classes/Digital/Digital.lvclass"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Generic.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/QSI/QControl Classes/Generic/Generic.lvclass"/>
				<Item Name="GObject.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/QSI/QControl Classes/GObject/GObject.lvclass"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVComboBoxStrsAndValuesArrayTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVComboBoxStrsAndValuesArrayTypeDef.ctl"/>
				<Item Name="LVDataSocketStatusTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDataSocketStatusTypeDef.ctl"/>
				<Item Name="LVFormatAndPrecisionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVFormatAndPrecisionTypeDef.ctl"/>
				<Item Name="LVKeyNavTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVKeyNavTypeDef.ctl"/>
				<Item Name="LVKeyTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVKeyTypeDef.ctl"/>
				<Item Name="LVMinMaxIncTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVMinMaxIncTypeDef.ctl"/>
				<Item Name="LVNumericRepresentation.ctl" Type="VI" URL="/&lt;vilib&gt;/numeric/LVNumericRepresentation.ctl"/>
				<Item Name="LVOutOfRangeActionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVOutOfRangeActionTypeDef.ctl"/>
				<Item Name="LVPoint32TypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPoint32TypeDef.ctl"/>
				<Item Name="LVPositionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPositionTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Numeric.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/QSI/QControl Classes/Numeric/Numeric.lvclass"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="String.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/QSI/QControl Classes/String/String.lvclass"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
